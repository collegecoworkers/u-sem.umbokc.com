<script src="//cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
{!! Form::open(['url' => isset($model) ? '/lesson/update/'.$model->id : '/lesson/create', 'class' => '', 'role' => 'form']) !!}
	<br>
  <div class="form-group"><label>Название</label><div>{!! Form::text('title', isset($model) ? $model->title : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Категория</label><div>{!! Form::select('cat_id', $cats, isset($model) ? $model->cat_id : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Описание</label><div>{!! Form::textArea('desc', isset($model) ? $model->desc : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Контент</label><div>{!! Form::textArea('data', isset($model) ? $model->data : '', ['class' => 'form-control', 'id' => 'editor1']) !!}</div></div>
	<button type="submit" class="btn btn-purple waves-effect waves-light">Отправить</button>
{!! Form::close() !!}
<script>CKEDITOR.replace( 'editor1' );</script>
