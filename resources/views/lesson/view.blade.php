@php
	use App\{
		Cat,
		User,
		Test,
		Testitem,
		Completslesson
	};
	$is_admin = User::isAdmin();
@endphp

@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-8">
		<div class="card-box">
			<h2 class=""><b>Урок: {{ $model->title }}</b></h2>
			<br>
			<p>
				{!! $model->data !!}
			</p>
			@php
				$completslesson = Completslesson::getBy(['user_id' => User::curr()->id, 'lesson_id' => $model->id]);
			@endphp
			@if (!$completslesson)
				<p>
					<a href="{{ route('/test/view/{id}', ['id' => $model->id]) }}" class="btn btn-default waves-effect waves-light">Пройти тест</a>
				</p>
			@else
				<p class="text-success">Тест пройден на: {{ $completslesson->test_percent }}%</p>
			@endisset
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card-box">
			<h4 class=" m-t-0 header-title"><b>Категории</b></h4>
			<br>
			@foreach ($cats as $item)
				<ul>
					<li><a href="{{ route('/lessons/by/{id}', ['id' => $item->id]) }}">{{$item->title}}</a></li>
				</ul>
			@endforeach
		</div>
	</div>
</div>
@endsection

