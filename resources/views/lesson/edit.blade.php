@extends('../layouts.app')
@section('content')
<div class="col-sm-12">
  <div class="card-box">
    <h4 class=" m-t-0 header-title"><b>Изменить урок</b></h4>
    @include('lesson._form')
  </div>
</div>
@endsection
