@php use App\{User,Test,Testitem}; $is_admin = User::isAdmin(); @endphp

@extends('layouts.app')
@section('content')
@if ($is_admin)
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<h4 class=" m-t-0 header-title"><b>Уроки</b></h4>
				<table class="table m-0">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Пользователь</th>
							<th>Тесты</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($lessons as $item)
							<tr>
								<td>{{$item->id}}</td>
								<td>{{$item->title}}</td>
								<td>{{ User::getById($item->user_id)->name }}</td>
								<td>
									@php
									$test = Test::getBy('lesson_id', $item->id);
									echo '<a href="'.route('/tests/all/{id}', ['id' => $item->id]).'">'.(($test) ? count(Testitem::getsBy('test_id', $test->id)) : 0).' <i class="md md-remove-red-eye"></i></a>  ';
									@endphp
								</td>
								<td>
									<a href="{{ route('/lesson/view/{id}', ['id' => $item->id]) }}">
										<i class="md md-remove-red-eye"></i>
									</a>
									<a href="{{ route('/lesson/edit/{id}', ['id'=>$item->id]) }}">
										<i class="md md-edit"></i>
									</a>
									<a href="{{ route('/lesson/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
										<i class="md md-delete"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@else

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 ">
			<div class="portfolioFilter">
				@isset ($cats)
					<a href="#" data-filter="*" class="current">Все</a>
					@foreach ($cats as $item)
						<a href="#" data-filter=".cat-{{$item->id}}">{{$item->title}}</a>
					@endforeach
				@endisset
				@isset ($cat)
					<a href="#" class="current" data-filter=".cat-{{$cat->id}}">{{$cat->title}}</a>
				@endisset
			</div>
		</div>
	</div>
	<div class="row port m-b-20">
		<div class="portfolioContainer">
			@foreach ($lessons as $item)
				<div class="col-sm-6 col-lg-3 col-md-4 cat-{{$item->cat_id}}">
					<div class="gal-detail thumb">
						<a href="{{ route('/lesson/view/{id}',['id' => $item->id]) }}" class="" title="">
							{{$item->desc}}
							<h4>{{$item->title}}</h4>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>

@endif
@endsection

