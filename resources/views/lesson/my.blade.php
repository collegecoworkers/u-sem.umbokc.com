@php
	use App\{User,Cat,Test,Testitem};
	$is_admin = User::isAdmin();
@endphp

@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{ route('/lesson/add') }}" class="btn btn-success waves-effect waves-light">Добавить</a>
		<br><br>
		<div class="card-box">
			<h4 class=" m-t-0 header-title"><b>Уроки</b></h4>
			<table class="table m-0">
				<thead>
					<tr>
						<th>#</th>
						<th>Название</th>
						<th>Категория</th>
						<th>Тесты</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($lessons as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>{{ Cat::getById($item->cat_id)->title }}</td>
							<td>
								@php
									$test = Test::getBy('lesson_id', $item->id);
									echo '<a href="'.route('/tests/all/{id}', ['id' => $item->id]).'">'.(($test) ? count(Testitem::getsBy('test_id', $test->id)) : 0).' <i class="md md-remove-red-eye"></i></a>  ';
									echo '<a href="'.route('/test/add/{id}', ['id' => $item->id]).'"><i class="md md-add"></i></a>';
								@endphp
							</td>
							<td>
								<a href="{{ route('/lesson/view/{id}', ['id' => $item->id]) }}">
									<i class="md md-remove-red-eye"></i>
								</a>
								<a href="{{ route('/lesson/edit/{id}', ['id'=>$item->id]) }}">
									<i class="md md-edit"></i>
								</a>
								<a href="{{ route('/lesson/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
									<i class="md md-delete"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

