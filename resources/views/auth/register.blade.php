@extends('../layouts.auth')
@section('content')
<div class="wrapper-page">
	<div class=" card-box">
		<div class="panel-heading"> 
			<h3 class="text-center"><strong class="text-custom">{{ config('app.name', 'Laravel') }}</strong><br>Регистрация</h3>
		</div> 
		<div class="panel-body">
			<form class="form-horizontal m-t-20" method="post" action="{{ route('register') }}">
				{{ csrf_field() }}

				<div class="form-group ">
					<div class="col-xs-12">
						<input name="full_name" placeholder="Полное имя" type="text" class="form-control" />
						@if ($errors->has('full_name'))<span class="help-block"><strong c#0>{{ $errors->first('full_name') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group ">
					<div class="col-xs-12">
						<input name="name" placeholder="Логин" type="text" class="form-control" />
						@if ($errors->has('name'))<span class="help-block"><strong c#0>{{ $errors->first('name') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group ">
					<div class="col-xs-12">
						<input name="email" placeholder="Email" type="text" class="form-control" />
						@if ($errors->has('email'))<span class="help-block"><strong c#0>{{ $errors->first('email') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-12">
						<input name="password" placeholder="Пароль" type="password" class="form-control" />
						@if ($errors->has('password'))<span class="help-block"><strong c#0>{{ $errors->first('password') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-12">
						<input name="password_confirmation" placeholder="Пароль еще раз" type="password" class="form-control" />
						@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#0>{{ $errors->first('password_confirmation') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group text-center m-t-40">
					<div class="col-xs-12">
						<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Отправить</button>
					</div>
				</div>
			</form> 

		</div>   
	</div>                              
	<div class="row">
		<div class="col-sm-12 text-center">
			<p><a href="{{ route('login') }}" class="text-primary m-l-5"><b>Войти</b></a></p>
		</div>
	</div>
</div>
@endsection
