@extends('../layouts.auth')
@section('content')
<div class="wrapper-page">
	<div class=" card-box">
		<div class="panel-heading"> 
			<h3 class="text-center"><strong class="text-custom">{{ config('app.name', 'Laravel') }}</strong><br>Войти</h3>
		</div> 


		<div class="panel-body">
			<form class="form-horizontal m-t-20" method="post" action="{{ route('login') }}">
				{{ csrf_field() }}
				<div class="form-group ">
					<div class="col-xs-12">
						<input name="email" placeholder="Email" type="text" class="form-control" />
						@if ($errors->has('email'))<span class="help-block"><strong c#0>{{ $errors->first('email') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-12">
						<input name="password" placeholder="Пароль" type="password" class="form-control" />
						@if ($errors->has('password'))<span class="help-block"><strong c#0>{{ $errors->first('password') }}</strong></span>@endif
					</div>
				</div>

				<div class="form-group text-center m-t-40">
					<div class="col-xs-12">
						<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Войти</button>
					</div>
				</div>
			</form> 

		</div>   
	</div>                              
	<div class="row">
		<div class="col-sm-12 text-center">
			<p><a href="{{ route('register') }}" class="text-primary m-l-5"><b>Регистрация</b></a></p>
		</div>
	</div>
</div>
@endsection
