@php use App\User; $is_admin = User::isAdmin(); @endphp

@extends('layouts.app')
@section('content')
<div class="row">
	@if ($is_admin)
		<div class="col-sm-12">
			<a href="{{ route('/cat/add') }}" class="btn btn-success waves-effect waves-light">Добавить</a>
			<br><br>
			<div class="card-box">
				<h4 class=" m-t-0 header-title"><b>Категории</b></h4>
				<table class="table m-0">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($cats as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>
								<a href="{{ route('/lessons/by/{id}', ['id' => $item->id]) }}">
									<i class="md md-remove-red-eye"></i>
								</a>
								<a href="{{ route('/cat/edit/{id}', ['id'=>$item->id]) }}">
									<i class="md md-edit"></i>
								</a>
								<a href="{{ route('/cat/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
									<i class="md md-delete"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	@else
		<div class="col-sm-12">
			<div class="card-box">
				<h4 class=" m-t-0 header-title"><b>Категории</b></h4>
				<div class="owl-carousel owl-theme owl-multi">
					@foreach ($cats as $item)
						<div class="item">
							<a href="{{ route('/lessons/by/{id}', ['id' => $item->id]) }}">
								<div class="card-box widget-box-1 bg-white">
									<h4 class="text-dark">{{ $item->title }}</h4>
									<h2 class="text-primary text-center">
										<span>{{ $item->counteLesson() }}</span>
									</h2>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
