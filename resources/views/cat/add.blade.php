@extends('../layouts.app')
@section('content')
<div class="col-sm-12">
	<div class="card-box">
		<h4 class=" m-t-0 header-title"><b>Добавить категорию</b></h4>
		@include('cat._form')
	</div>
</div>
@endsection
