{!! Form::open(['url' => isset($model) ? '/cat/update/'.$model->id : '/cat/create', 'class' => '', 'role' => 'form']) !!}
	<br>
	<div class="form-group"><label>Название</label><div>{!! Form::text('title', isset($model) ? $model->title : '', ['class' => 'form-control']) !!}</div></div>
	<button type="submit" class="btn btn-purple waves-effect waves-light">Отправить</button>
{!! Form::close() !!}
