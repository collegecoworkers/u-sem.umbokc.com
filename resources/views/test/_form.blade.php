{!! Form::open(['url' => isset($model) ? '/test/update/'.$model->id : '/test/create/'.$id, 'role' => 'form']) !!}
	<br>
  <div class="form-group"><label>Вопрос</label><div>{!! Form::text('question', isset($model) ? $model->question : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Ответ 1</label><div>{!! Form::text('answer1', isset($model) ? $model->answer1 : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Ответ 2</label><div>{!! Form::text('answer2', isset($model) ? $model->answer2 : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Ответ 3</label><div>{!! Form::text('answer3', isset($model) ? $model->answer3 : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>Ответ 4</label><div>{!! Form::text('answer4', isset($model) ? $model->answer4 : '', ['class' => 'form-control']) !!}</div></div>
  <div class="form-group"><label>№ Ответа</label><div>{!! Form::select('true', ['1' => '1','2' => '2','3' => '3','4' => '4'], isset($model) ? $model->true : '', ['class' => 'form-control']) !!}</div></div>
	<button type="submit" class="btn btn-purple waves-effect waves-light">Отправить</button>
{!! Form::close() !!}
