@php
	use App\{User,Cat,Test,Testitem};
	$is_admin = User::isAdmin();
@endphp

@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<h2 class=""><b>Тесты урока: {{ $model->title }}</b></h2>
			@if (isset($testitems) && !empty($testitems) && count($testitems) > 0)
			{!! Form::open(['url' => '/test/check/'.$model->id, 'role' => 'form']) !!}
				<br>
				@foreach ($testitems as $item)
					<div class="form-group">
						<label>{{$item->question}}</label><br>
						@php
							$name = 'testitem['.$item->id.']';
						@endphp
						<div>{!! Form::select($name, $item->answArr(), '', ['class' => 'form-control']) !!}</div>
					</div>
				@endforeach
				<button type="submit" class="btn btn-purple waves-effect waves-light">Отправить</button>
			{!! Form::close() !!}
			@else
			<p>Тесто нет</p>
			@endif
		</div>
	</div>
</div>
@endsection

