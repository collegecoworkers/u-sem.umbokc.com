@php use App\User; $is_admin = User::isAdmin(); @endphp

@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{ route('/test/add/{id}', ['id' => $model->id]) }}" class="btn btn-success waves-effect waves-light">Добавить</a>
		<br><br>
		<div class="card-box">
			<h4 class=" m-t-0 header-title"><b>Тесты урока: {{ $model->title }}</b></h4>
			<table class="table m-0">
				<thead>
					<tr>
						<th>#</th>
						<th>Вопрос</th>
						<th>Ответ</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($testitems as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->question}}</td>
							<td>{{ $item->getAnswer() }}</td>
							<td>
								<a href="{{ route('/test/edit/{id}', ['id'=>$model->id]) }}">
									<i class="md md-edit"></i>
								</a>
								<a href="{{ route('/test/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
									<i class="md md-delete"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

