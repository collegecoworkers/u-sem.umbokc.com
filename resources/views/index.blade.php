@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-md-6 col-lg-4">
		<div class="widget-bg-color-icon card-box fadeInDown animated">
			<div class="bg-icon bg-icon-info pull-left">
				<i class="md md-equalizer text-info"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="">{{ count($completslessons) }}</b></h3>
				<p class="text-muted">Завершенных уроков</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-6 col-lg-4">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-pink pull-left">
				<i class="md md-done-all text-pink"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="">{{ $completslesson_max }}</b></h3>
				<p class="text-muted">Самый высокий балл</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-6 col-lg-4">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-success pull-left">
				<i class="md md-remove-red-eye text-success"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="">{{ $lessons }}</b></h3>
				<p class="text-muted">Всего уроков</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	@if ($is_teacher)
	<div class="col-md-6 col-lg-4">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-purple pull-left">
				<i class="md md-equalizer text-purple"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="">{{ $your_lessons }}</b></h3>
				<p class="text-muted">Ваших уроков</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	@endif
</div>
@endsection
