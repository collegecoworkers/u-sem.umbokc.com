{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create', 'class' => '', 'role' => 'form']) !!}
<br>
<div class="form-group"><label>Имя</label><div>{!! Form::text('full_name', isset($model) ? $model->full_name : '', ['required' => '', 'class' => 'form-control']) !!}</div></div>
<div class="form-group"><label>Логин</label><div>{!! Form::text('name', isset($model) ? $model->name : '', ['required' => '', 'class' => 'form-control']) !!}</div></div>
<div class="form-group"><label>Email</label><div>{!! Form::text('email', isset($model) ? $model->email : '', ['required' => '', 'class' => 'form-control']) !!}</div></div>
<div class="form-group"><label>Роль</label><div>{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '', 'class' => 'form-control']) !!}</div></div>
<div class="form-group"><label>Пароль</label><div>{!! Form::password('password', ['class' => 'form-control']) !!}</div></div>
<button type="submit" class="btn btn-purple waves-effect waves-light">Отправить</button>
{!! Form::close() !!}
