@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{ route('/user/add') }}" class="btn btn-success waves-effect waves-light">Добавить</a>
		<br><br>
		<div class="card-box">
			<h4 class=" m-t-0 header-title"><b>Пользователи</b></h4>
			<table class="table m-0">
				<thead>
					<tr>
						<th>#</th>
						<th>Имя</th>
						<th>Логин</th>
						<th>Email</th>
						<th>Статус</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $item)
					<tr>
						<td>{{$item->id}}</td>
						<td>{{$item->full_name}}</td>
						<td>{{$item->name}}</td>
						<td>{{$item->email}}</td>
						<td>{{$item->getRole()}}</td>
						<td>
							<a href="{{ route('/user/edit/{id}', ['id'=>$item->id]) }}">
								<i class="md md-edit"></i>
							</a>
							<a href="{{ route('/user/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
								<i class="md md-delete"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
