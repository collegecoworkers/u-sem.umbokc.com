@extends('../layouts.app')
@section('content')
<div class="col-sm-12">
	<div class="card-box">
		<h4 class=" m-t-0 header-title"><b>Добавить пользователя</b></h4>
		@include('user._form')
	</div>
</div>
@endsection
