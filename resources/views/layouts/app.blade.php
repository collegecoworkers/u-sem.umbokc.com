@php
use App\User;
@endphp
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="/assets/images/favicon_1.ico">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<link rel="stylesheet" href="/assets/plugins/morris/morris.css">

	<link href="/assets/plugins/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/plugins/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/plugins/magnific-popup/css/magnific-popup.css" rel="stylesheet" />

	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/core.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/components.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/menu.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/responsive.css" rel="stylesheet" type="text/css" />

	<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="/assets/js/modernizr.min.js"></script>
</head>
<body>
	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container">

				<div class="logo">
					<a href="/" class="logo">{{ config('app.name', 'Laravel') }}</a>
				</div>

				<div class="menu-extras">
					<ul class="nav navbar-nav navbar-right pull-right">
						<li class="dropdown navbar-c-items">
							<a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true">
								<span>{{ User::curr()->full_name }}</span>
							</a>
							<ul class="dropdown-menu">
								{{-- <li><a href="javascript:void(0)"><i class="ti-user text-custom m-r-10"></i> Profile</a></li> --}}
								<li class="divider"></li>
								<li><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ti-power-off text-danger m-r-10"></i> Выйти</a></li>
							</ul>
						</li>
					</ul>
					<div class="menu-item">
						<a class="navbar-toggle">
							<div class="lines">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar-custom">
			<div class="container">
				<div id="navigation">
					<ul class="navigation-menu">
						@include('layouts.nav')
					</ul>
				</div>
			</div>
		</div>
	</header>

	<div class="wrapper">
		<div class="container">

			@yield('content')

			<footer class="footer text-right">
				<div class="container">
					<div class="row">
						<div class="col-xs-6">
							&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/detect.js"></script>
	<script src="/assets/js/fastclick.js"></script>

	<script src="/assets/js/jquery.slimscroll.js"></script>
	<script src="/assets/js/jquery.blockUI.js"></script>
	<script src="/assets/js/waves.js"></script>
	<script src="/assets/js/wow.min.js"></script>
	<script src="/assets/js/jquery.nicescroll.js"></script>
	<script src="/assets/js/jquery.scrollTo.min.js"></script>

	<script src="/assets/plugins/peity/jquery.peity.min.js"></script>

	<script src="/assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
	<script src="/assets/plugins/counterup/jquery.counterup.min.js"></script>

	<script src="/assets/plugins/morris/morris.min.js"></script>
	<script src="/assets/plugins/raphael/raphael-min.js"></script>

	<script src="/assets/plugins/isotope/js/isotope.pkgd.min.js"></script>
	<script src="/assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

	<script src="/assets/plugins/jquery-knob/jquery.knob.js"></script>

	<script src="/assets/pages/jquery.dashboard.js"></script>
	<script src="/assets/plugins/owl.carousel/dist/owl.carousel.min.js"></script>
	<script src="/assets/js/jquery.core.js"></script>
	<script src="/assets/js/jquery.app.js"></script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.counter').counterUp({
				delay: 100,
				time: 1200
			});

			$(".knob").knob();

			$('.owl-multi').owlCarousel({
				margin:20, nav:false, autoplay:true,
				responsive:{0:{items:1},480:{items:2},700:{items:4},1000:{items:3},1100:{items:5}}
			});
		});
		$(window).load(function () {
			var $container = $('.portfolioContainer');
			$container.isotope({
				filter: '*',
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});

			$('.portfolioFilter a').click(function () {
				$('.portfolioFilter .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
				$container.isotope({
					filter: selector,
					animationOptions: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
				return false;
			});
		});
		$(document).ready(function () {
			$('.image-popup').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-fade',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0, 1]
				}
			});
		});
	</script>
</body>
</html>
