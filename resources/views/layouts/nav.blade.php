@php
	use App\User;
	$r = User::curRole();
	$nav = [
		['visible' => true, 'url' => route('/'), 'icon' => 'home', 'label' => 'Главная'],
		['visible' => true, 'url' => route('/cats'), 'icon' => 'layers', 'label' => 'Категории'],
		['visible' => true, 'url' => route('/lessons'), 'icon' => 'folder-special', 'label' => 'Уроки'],
		['visible' => $r == 'teacher', 'url' => route('/lessons/my'), 'icon' => 'folder-special', 'label' => 'Мои уроки'],
		['visible' => $r == 'admin', 'url' => route('/users'), 'icon' => 'group', 'label' => 'Пользователи'],
	];
@endphp

@foreach ($nav as $item)
	@if ($item['visible'])
		<li class="{{ url()->current() == $item['url'] ? 'active' : ''}}">
			<a href="{{ $item['url'] }}">
				<i class="md md-{{$item['icon']}}"></i>
				{{$item['label']}}
			</a>
		</li>
	@endif
@endforeach

