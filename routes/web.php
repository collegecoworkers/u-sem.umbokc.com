<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');

Route::get('/cats', 'CatController@Index')->name('/cats');
Route::get('/cat/add', 'CatController@Add')->name('/cat/add');
Route::get('/cat/edit/{id}', 'CatController@Edit')->name('/cat/edit/{id}');
Route::get('/cat/delete/{id}', 'CatController@Delete')->name('/cat/delete/{id}');
Route::post('/cat/create', 'CatController@Create')->name('/cat/create');
Route::post('/cat/update/{id}', 'CatController@Update')->name('/cat/update/{id}');

Route::get('/lessons', 'LessonController@Index')->name('/lessons');
Route::get('/lessons/my', 'LessonController@My')->name('/lessons/my');
Route::get('/lessons/by/{id}', 'LessonController@By')->name('/lessons/by/{id}');
Route::get('/lesson/view/{id}', 'LessonController@View')->name('/lesson/view/{id}');
Route::get('/lesson/add', 'LessonController@Add')->name('/lesson/add');
Route::get('/lesson/edit/{id}', 'LessonController@Edit')->name('/lesson/edit/{id}');
Route::get('/lesson/delete/{id}', 'LessonController@Delete')->name('/lesson/delete/{id}');
Route::post('/lesson/create', 'LessonController@Create')->name('/lesson/create');
Route::post('/lesson/update/{id}', 'LessonController@Update')->name('/lesson/update/{id}');

Route::get('/tests/all/{id}', 'TestController@Index')->name('/tests/all/{id}');
Route::get('/test/add/{id}', 'TestController@Add')->name('/test/add/{id}');
Route::get('/test/view/{id}', 'TestController@View')->name('/test/view/{id}');
Route::get('/test/edit/{id}', 'TestController@Edit')->name('/test/edit/{id}');
Route::get('/test/delete/{id}', 'TestController@Delete')->name('/test/delete/{id}');
Route::post('/test/check/{id}', 'TestController@Check')->name('/test/check/{id}');
Route::post('/test/create/{id}', 'TestController@Create')->name('/test/create/{id}');
Route::post('/test/update/{id}', 'TestController@Update')->name('/test/update/{id}');

Route::get('/users', 'UserController@Index')->name('/users');
Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
