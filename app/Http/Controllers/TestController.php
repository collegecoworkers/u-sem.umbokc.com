<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Cat,
	User,
	Test,
	Lesson,
	Testitem,
	Openlesson,
	Requestlesson,
	Completslesson
};

class TestController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index($id) {
		return view('test.index')->with([
			'model' => Lesson::getById($id),
			'testitems' => Testitem::getsBy('test_id', Test::getBy('lesson_id', Lesson::getById($id)->id)->id),
		]);
	}
	public function Check($id, Request $request) {
		$testitems = request()->testitem;
		$completslesson = new Completslesson();
		$count = count($testitems);
		$true = 0;

		foreach ($testitems as $key => $value) {
			$test = Testitem::getBy('id', $key);
			if($test->true == $value) $true++;
		}

		$completslesson->lesson_id = $id;
		$completslesson->user_id = User::curr()->id;
		$completslesson->test_percent = ceil($true / $count * 100);
		$completslesson->save();

		return redirect('/');
	}
	public function View($id) {
		$test = Test::getBy('lesson_id', $id);
		return view('test.view')->with([
			'model' => Lesson::getBy('id', $test->lesson_id),
			'testitems' => Testitem::getsBy('test_id', $test->id),
		]);
	}
	public function Add($id) {
		return view('test.add')->with([
			'id' => $id,
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$model = Testitem::getBy('id', $id);
		return view('test.edit')->with([
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$test = Testitem::getBy('id', $id);
		Testitem::where('id', $id)->delete();
		return redirect()->to('/tests/all/'.$test->lesson_id);
	}
	public function Create($id, Request $request) {
		$test = Test::getBy('lesson_id', $id);
		$model = new Testitem();

		$model->question = request()->question;
		$model->answer1 = request()->answer1;
		$model->answer2 = request()->answer2;
		$model->answer3 = request()->answer3;
		$model->answer4 = request()->answer4;
		$model->true = request()->true;
		$model->test_id = $test->id;

		$model->save();

		return redirect()->to('/tests/all/'.$test->lesson_id);
	}
	public function Update($id, Request $request) {

		$test = Test::getBy('lesson_id', $id);

		$model = Testitem::getBy('id', $id);

		$model->question = request()->question;
		$model->answer1 = request()->answer1;
		$model->answer2 = request()->answer2;
		$model->answer3 = request()->answer3;
		$model->answer4 = request()->answer4;
		$model->true = request()->true;

		$model->save();
		return redirect()->to('/tests/all/'.$test->lesson_id);
	}
}
