<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Cat,
	User,
	Test,
	Lesson,
	Testitem,
	Openlesson,
	Requestlesson,
	Completslesson
};

class CatController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		return view('cat.index')->with([
			'cats' => Cat::all(),
		]);
	}
	public function Add() {
		return view('cat.add')->with([
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$model = Cat::getBy('id', $id);
		return view('cat.edit')->with([
			'model' => $model,
		]);
	}
	public function Delete($id) {
		Cat::where('id', $id)->delete();
		return redirect()->to('/cats');
	}
	public function Create(Request $request) {
		$model = new Cat();

		$model->title = request()->title;

		$model->save();
		return redirect()->to('/cats');
	}
	public function Update($id, Request $request) {
		$model = Cat::getBy('id', $id);

		$model->title = request()->title;

		$model->save();
		return redirect()->to('/cats');
	}
}
