<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Cat,
	User,
	Test,
	Lesson,
	Testitem,
	Openlesson,
	Requestlesson,
	Completslesson
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$completslessons = Completslesson::getsBy('user_id', User::curr()->id);
		$completslesson_max = 0;
		foreach ($completslessons as $item) {
			if($item->test_percent > $completslesson_max)
				$completslesson_max = $item->test_percent;
		}
		return view('index')->with([
			'is_teacher' => User::curRole() == 'teacher',
			'lessons' => Lesson::count(),
			'your_lessons' => count(Lesson::getsBy('user_id', User::curr()->id)),
			'completslessons' => $completslessons,
			'completslesson_max' => $completslesson_max,
		]);
	}
}
