<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Cat,
	User,
	Test,
	Lesson,
	Testitem,
	Openlesson,
	Requestlesson,
	Completslesson
};

class LessonController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		return view('lesson.index')->with([
			'lessons' => Lesson::all(),
			'cats' => Cat::all(),
		]);
	}
	public function By($id) {
		$cat = Cat::getById($id);
		return view('lesson.index')->with([
			'lessons' => Lesson::getsBy('cat_id', $cat->id),
			'cat' => $cat,
		]);
	}
	public function My() {
		$user = User::curr();
		return view('lesson.my')->with([
			'lessons' => Lesson::getsBy('user_id', $user->id),
		]);
	}
	public function View($id) {
		$model = Lesson::getBy('id', $id);
		return view('lesson.view')->with([
			'model' => $model,
			'cats' => Cat::all(),
		]);
	}
	public function Add() {
		return view('lesson.add')->with([
			'user' => User::curr(),
			'cats' => Cat::allArr(),
		]);
	}
	public function Edit($id) {
		$model = Lesson::getBy('id', $id);
		return view('lesson.edit')->with([
			'model' => $model,
			'cats' => Cat::allArr(),
		]);
	}
	public function Delete($id) {
		Lesson::where('id', $id)->delete();
		return redirect()->to('/lessons');
	}
	public function Create(Request $request) {
		$model = new Lesson();

		$model->title = request()->title;
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->data = request()->data;
		$model->video = '';
		$model->user_id = User::curr()->id;

		$model->save();

		$test = new Test();
		$test->lesson_id = $model->id;
		$test->save();

		return redirect()->to('/lessons');
	}
	public function Update($id, Request $request) {
		$model = Lesson::getBy('id', $id);

		$model->title = request()->title;
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->data = request()->data;

		$model->save();
		return redirect()->to('/lessons');
	}
}
